package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/minio-cli/replication"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "minio",
		Short: "OPS tools for minio",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(replication.NewCommand())
}
