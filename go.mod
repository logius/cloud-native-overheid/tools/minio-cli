module gitlab.com/logius/cloud-native-overheid/tools/minio-cli

go 1.16

require (
	github.com/minio/madmin-go v1.0.20
	github.com/minio/minio-go/v7 v7.0.12
	github.com/spf13/cobra v1.1.1
	k8s.io/apimachinery v0.22.0
	k8s.io/client-go v0.22.0
)
