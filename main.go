package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/minio-cli/commands"
)

func main() {
	rootCmd := &cobra.Command{}
	commands.AddSubCommands(rootCmd)
	rootCmd.SilenceUsage = true
	err := rootCmd.Execute()

	if err != nil {
		os.Exit(1)
	}
}
