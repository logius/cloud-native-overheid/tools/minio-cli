package replication

import (
	"context"
	"log"
	"strings"

	"github.com/minio/madmin-go"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio-go/v7/pkg/lifecycle"
	"github.com/minio/minio-go/v7/pkg/replication"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/minio-cli/secrets"
)

type flags struct {
	minioSourceEndpoint *string
	minioTargetEndpoint *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureReplication(cmd, &flags)
		},
		Use:   "configure-replication",
		Short: "Configure minio replication",
		Long:  "This command configures bucket replication in minio.",
	}

	flags.minioSourceEndpoint = cmd.Flags().String("source-endpoint", "", "minio Source Endpoint")
	flags.minioTargetEndpoint = cmd.Flags().String("target-endpoint", "", "minio Target Endpoint")

	cmd.MarkFlagRequired("source-endpoint")
	cmd.MarkFlagRequired("target-endpoint")

	return cmd
}

func configureReplication(cmd *cobra.Command, flags *flags) error {

	minioSourceClient, minioSourceAdmin, err := createSourceClients(*flags.minioSourceEndpoint)
	if err != nil {
		return err
	}

	replicationBuckets, noReplicationBuckets, err := getReplicationBuckets(minioSourceClient)
	if err != nil {
		return err
	}

	if err = clearReplication(minioSourceClient, minioSourceAdmin, noReplicationBuckets); err != nil {
		return err
	}

	if err := configureLifecycleForBuckets(minioSourceClient, replicationBuckets); err != nil {
		return err
	}

	if err := configureTargetSite(minioSourceClient, replicationBuckets, *flags.minioTargetEndpoint); err != nil {
		return err
	}

	for _, replicationBucket := range replicationBuckets {

		arn, err := configureTargetForBucket(minioSourceAdmin, *flags.minioTargetEndpoint, replicationBucket.Name)
		if err != nil {
			return err
		}
		if err := configureReplicationForBucket(minioSourceClient, arn, replicationBucket.Name); err != nil {
			return err
		}
	}
	return nil
}

func createSourceClients(minioSourceEndpoint string) (*minio.Client, *madmin.AdminClient, error) {

	accessKeyID, secretAccessKey := secrets.GetSourceMinioSecrets()

	return createClients(minioSourceEndpoint, accessKeyID, secretAccessKey)
}

// Create minio and admin client
func createClients(endpoint string, accessKeyID string, secretAccessKey string) (*minio.Client, *madmin.AdminClient, error) {
	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: true,
	})
	if err != nil {
		return nil, nil, err
	}
	madmClnt, err := madmin.New(endpoint, accessKeyID, secretAccessKey, true)
	if err != nil {
		return nil, nil, err
	}
	return minioClient, madmClnt, nil
}

// Gets buckets with a tag replication=true
func getReplicationBuckets(minioClient *minio.Client) ([]minio.BucketInfo, []minio.BucketInfo, error) {
	replicationBuckets := make([]minio.BucketInfo, 0)
	noReplicationBuckets := make([]minio.BucketInfo, 0)
	buckets, err := minioClient.ListBuckets(context.Background())
	if err != nil {
		return replicationBuckets, noReplicationBuckets, err
	}

	for _, bucket := range buckets {
		replication := false
		tags, err := minioClient.GetBucketTagging(context.Background(), bucket.Name)
		if err == nil {

			for key, value := range tags.ToMap() {
				if key == string(madmin.ReplicationService) && stringInSlice(strings.ToLower(value), []string{"yes", "true", "on"}) {
					replicationBuckets = append(replicationBuckets, bucket)
					replication = true
				}
			}
		}
		if !replication {
			noReplicationBuckets = append(noReplicationBuckets, bucket)
		}
	}
	return replicationBuckets, noReplicationBuckets, nil
}

// Configure ARN to the target bucket for the replication user
func configureTargetForBucket(minioAdmin *madmin.AdminClient, targetEndPoint string, bucketName string) (string, error) {

	log.Printf("List targets for bucket %q", bucketName)
	targets, err := minioAdmin.ListRemoteTargets(context.Background(), bucketName, string(madmin.ReplicationService))
	if err != nil {
		return "", err
	}

	if len(targets) > 0 {
		log.Printf("Target for bucket %q already exists: %q", bucketName, targets[0].Arn)
		return targets[0].Arn, nil
	}

	accesskey, secretkey := secrets.GetReplicationUserSecrets()
	replicationUserCreds := madmin.Credentials{
		AccessKey: accesskey,
		SecretKey: secretkey,
	}

	targetBucket := madmin.BucketTarget{

		Endpoint:     targetEndPoint,
		Credentials:  &replicationUserCreds,
		SourceBucket: bucketName,
		TargetBucket: bucketName,
		Secure:       true,
		Type:         madmin.ReplicationService,
	}

	log.Printf("Set remote target for bucket %q using %q", bucketName, accesskey)
	return minioAdmin.SetRemoteTarget(context.Background(), bucketName, &targetBucket)
}

func configureLifecycleForBuckets(minioClient *minio.Client, replicationBuckets []minio.BucketInfo) error {

	config := lifecycle.NewConfiguration()
	config.Rules = []lifecycle.Rule{
		{
			ID:     "remove-noncurrent-versions",
			Status: "Enabled",
			NoncurrentVersionExpiration: lifecycle.NoncurrentVersionExpiration{
				NoncurrentDays: 1,
			},
		},
		{
			ID:     "remove-delete-marker",
			Status: "Enabled",
			Expiration: lifecycle.Expiration{
				DeleteMarker: true,
			},
		},
	}
	for _, replicationBucket := range replicationBuckets {

		log.Printf("Setting ILM for bucket %q", replicationBucket.Name)

		var newCfg = lifecycle.NewConfiguration()
		*newCfg = *config

		currentCfg, err := minioClient.GetBucketLifecycle(context.Background(), replicationBucket.Name)
		if err == nil {
			for _, currentCfgRule := range currentCfg.Rules {
				var found = false
				for _, rule := range config.Rules {
					if rule.ID == currentCfgRule.ID {
						found = true
					}
				}
				if !found {
					log.Printf("Found existing ILM rule %q ", currentCfgRule.ID)
					newCfg.Rules = append(newCfg.Rules, currentCfgRule)
				}
			}
		}

		err = minioClient.SetBucketLifecycle(context.Background(), replicationBucket.Name, newCfg)
		if err != nil {
			return err
		}
	}
	return nil
}

// Configure replication in the bucket using the created ARN
func configureReplicationForBucket(minioClient *minio.Client, arn string, bucketName string) error {

	replicationCfg, err := minioClient.GetBucketReplication(context.Background(), bucketName)
	if err != nil {
		return err
	}
	if replicationCfg.Role == arn {
		log.Printf("Replication for bucket %q already exists", bucketName)
		return nil
	}

	cfg := replication.Config{
		Rules: []replication.Rule{
			{
				DeleteReplication: replication.DeleteReplication{
					Status: replication.Enabled,
				},
				DeleteMarkerReplication: replication.DeleteMarkerReplication{
					Status: replication.Enabled,
				},
				Destination: replication.Destination{
					Bucket: "arn:aws:s3:::" + bucketName,
				},
				ExistingObjectReplication: replication.ExistingObjectReplication{
					Status: replication.Enabled,
				},
				SourceSelectionCriteria: replication.SourceSelectionCriteria{
					ReplicaModifications: replication.ReplicaModifications{
						Status: replication.Enabled,
					},
				},
				Status: replication.Enabled,
			},
		},
		Role: arn,
	}

	log.Printf("Set bucket replication for %q", bucketName)
	err = minioClient.SetBucketReplication(context.Background(), bucketName, cfg)
	if err != nil {
		return err
	}
	return nil
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
