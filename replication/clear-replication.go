package replication

import (
	"context"
	"log"

	"github.com/minio/madmin-go"
	"github.com/minio/minio-go/v7"
)

func clearReplication(minioClient *minio.Client, minioAdmin *madmin.AdminClient, noReplicationBuckets []minio.BucketInfo) error {

	for _, bucket := range noReplicationBuckets {

		replicationCfg, err := minioClient.GetBucketReplication(context.Background(), bucket.Name)
		if err != nil {
			return err
		}

		if !replicationCfg.Empty() {

			log.Printf("Clear replication from %q", bucket.Name)

			err := minioClient.RemoveBucketReplication(context.Background(), bucket.Name)
			if err != nil {
				return err
			}
		}

		targets, err := minioAdmin.ListRemoteTargets(context.Background(), bucket.Name, string(madmin.ReplicationService))
		if err != nil {
			return err
		}
		for _, target := range targets {
			log.Printf("Remove target from %q", bucket.Name)
			if err := minioAdmin.RemoveRemoteTarget(context.Background(), bucket.Name, target.Arn); err != nil {
				return err
			}
		}
	}

	return nil
}
