package replication

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/minio/madmin-go"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio-go/v7/pkg/lifecycle"
	"gitlab.com/logius/cloud-native-overheid/tools/minio-cli/secrets"
)

func configureTargetSite(minioSourceClient *minio.Client, replicationBuckets []minio.BucketInfo, minioTargetEndpoint string) error {
	minioTargetClient, minioTargetAdmin, err := createTargetClients(minioTargetEndpoint)
	if err != nil {
		return err
	}

	for _, replicationBucket := range replicationBuckets {
		lifeCycleConfiguration, err := getLifecycle(minioSourceClient, replicationBucket.Name)
		if err != nil {
			return err
		}
		err = configureRemoteBucket(minioTargetClient, replicationBucket.Name, lifeCycleConfiguration)
		if err != nil {
			return err
		}
	}

	configurePolicy(minioTargetAdmin, replicationBuckets)
	if err := addReplicationUser(minioTargetAdmin, minioTargetEndpoint); err != nil {
		return err
	}

	return nil
}

func createTargetClients(minioSourceEndpoint string) (*minio.Client, *madmin.AdminClient, error) {

	accessKeyID, secretAccessKey := secrets.GetTargetMinioSecrets()

	return createClients(minioSourceEndpoint, accessKeyID, secretAccessKey)
}

// Gets the lifecycle of the bucket so we can clone it to the target site
func getLifecycle(minioClient *minio.Client, bucketName string) (*lifecycle.Configuration, error) {
	configuration, err := minioClient.GetBucketLifecycle(context.Background(), bucketName)

	if minioError, ok := err.(minio.ErrorResponse); ok {
		if minioError.Code == "NoSuchLifecycleConfiguration" {
			return nil, nil
		}
	}
	return configuration, err
}

// Make sure bucket exists and has a proper config for versioning and lifecycle
func configureRemoteBucket(minioClient *minio.Client, bucketName string, lifecycleConfiguration *lifecycle.Configuration) error {
	exists, err := minioClient.BucketExists(context.Background(), bucketName)
	if err != nil {
		return err
	}

	if !exists {
		log.Printf("Make target replication bucket %q in site %q", bucketName, minioClient.EndpointURL())
		err := minioClient.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{})
		if err != nil {
			return err
		}
	}

	log.Printf("Enable versioning on bucket %q", bucketName)
	err = minioClient.EnableVersioning(context.Background(), bucketName)
	if err != nil {
		return err
	}

	if lifecycleConfiguration != nil {
		var sb strings.Builder
		for _, rule := range lifecycleConfiguration.Rules {
			if sb.Len() > 0 {
				sb.WriteString(", ")
			}
			sb.WriteString(rule.ID)
		}
		log.Printf("Set lifecycleConfiguration %s for bucket %q", sb.String(), bucketName)
		return minioClient.SetBucketLifecycle(context.Background(), bucketName, lifecycleConfiguration)
	}
	return nil
}

func addReplicationUser(minioAdmin *madmin.AdminClient, targetEndPoint string) error {

	accesskey, secretkey := secrets.GetReplicationUserSecrets()

	if err := minioAdmin.AddUser(context.Background(), accesskey, secretkey); err != nil {
		return err
	}
	if err := minioAdmin.SetPolicy(context.Background(), "replicate", accesskey, false); err != nil {
		return err
	}

	return validateReplicationUser(targetEndPoint, accesskey, secretkey)
}

func validateReplicationUser(targetEndPoint string, accesskey string, secretkey string) error {
	minioClient, err := minio.New(targetEndPoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accesskey, secretkey, ""),
		Secure: true,
	})
	if err == nil {
		_, err = minioClient.ListBuckets(context.Background())
		if err == nil {
			log.Printf("Replication user %q validated: OK", accesskey)
			return nil
		}
	}
	return fmt.Errorf("user %q does not have access to %q (%w)", accesskey, targetEndPoint, err)
}
