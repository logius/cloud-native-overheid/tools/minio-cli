package replication

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/minio/madmin-go"
	"github.com/minio/minio-go/v7"
)

type Statement struct {
	Action   []string
	Effect   string
	Resource []string
	Sid      string
}

type Policy struct {
	Statement []Statement
	Version   string
}

func configurePolicy(minioAdmin *madmin.AdminClient, replicationBuckets []minio.BucketInfo) error {

	policy := createReplicationPolicy(replicationBuckets)

	return minioAdmin.AddCannedPolicy(context.Background(), "replicate", policy)
}

func createReplicationPolicy(replicationBuckets []minio.BucketInfo) []byte {

	buckets := make([]string, 0)
	resourceObjects := make([]string, 0)
	for _, bucket := range replicationBuckets {
		buckets = append(buckets, fmt.Sprintf("arn:aws:s3:::%s", bucket.Name))
		resourceObjects = append(resourceObjects, fmt.Sprintf("arn:aws:s3:::%s/*", bucket.Name))
	}
	policy := Policy{
		Version: "2012-10-17",
		Statement: []Statement{
			{
				Effect: "Allow",
				Action: []string{
					"s3:GetReplicationConfiguration",
					"s3:ListBucket",
					"s3:ListBucketMultipartUploads",
					"s3:GetBucketLocation",
					"s3:GetBucketVersioning",
					"s3:GetBucketObjectLockConfiguration",
					"s3:GetEncryptionConfiguration",
				},
				Resource: buckets,
				Sid:      "EnableReplicationOnBucket",
			},
			{
				Effect: "Allow",
				Action: []string{
					"s3:GetReplicationConfiguration",
					"s3:ReplicateTags",
					"s3:AbortMultipartUpload",
					"s3:GetObject",
					"s3:GetObjectVersion",
					"s3:GetObjectVersionTagging",
					"s3:PutObject",
					"s3:PutObjectRetention",
					"s3:PutBucketObjectLockConfiguration",
					"s3:PutObjectLegalHold",
					"s3:DeleteObject",
					"s3:ReplicateObject",
					"s3:ReplicateDelete",
				},
				Resource: resourceObjects,
				Sid:      "EnableReplicatingDataIntoBucket",
			},
		},
	}

	b, err := json.Marshal(policy)
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	return b
}
