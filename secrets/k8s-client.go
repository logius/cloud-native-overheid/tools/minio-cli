package secrets

import (
	"log"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var k8sClient *kubernetes.Clientset

func initK8sClient() *kubernetes.Clientset {
	if k8sClient != nil {
		return k8sClient
	}
	kubeconfig := getConfig("")

	clientConfig, err := kubeconfig.ClientConfig()
	if err != nil {
		log.Fatal(err)
	}

	k8sClient = createClientset(clientConfig)
	return k8sClient
}

// getConfig returns a Kubernetes client config for a given context.
func getConfig(context string) clientcmd.ClientConfig {

	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	if context != "" {
		configOverrides.CurrentContext = context
	}
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)
}

// createClientset creates the K8s Client
func createClientset(config *rest.Config) *kubernetes.Clientset {

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	return clientset
}
