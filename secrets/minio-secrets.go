package secrets

import (
	"context"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func GetSourceMinioSecrets() (string, string) {

	return getKeys("minio-secret")
}

func GetTargetMinioSecrets() (string, string) {

	return getKeys("minio-secret-remote")
}

func GetReplicationUserSecrets() (string, string) {
	return getKeys("repl-user")
}

func getKeys(secretName string) (string, string) {
	k8sClient := initK8sClient()

	namespace := getMinioNamespace(k8sClient)

	secret, err := k8sClient.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if err != nil {
		log.Fatalf("%v", err)
	}

	accesskey := string(secret.Data["accesskey"])
	if accesskey == "" {
		log.Fatalf("Missing accesskey in secret %q", secret.Name)
	}
	secretkey := string(secret.Data["secretkey"])
	if secretkey == "" {
		log.Fatalf("Missing secretkey in secret %q", secret.Name)
	}
	return accesskey, secretkey
}

func getMinioNamespace(k8sClient *kubernetes.Clientset) string {

	opts := metav1.ListOptions{
		LabelSelector: "v1.min.io/tenant = minio",
	}
	statefulSets, err := k8sClient.AppsV1().StatefulSets("").List(context.TODO(), opts)
	if err != nil || len(statefulSets.Items) == 0 {
		log.Fatalf("Could not find minio statefulset by label %q", opts.LabelSelector)
	}
	return statefulSets.Items[0].Namespace
}
